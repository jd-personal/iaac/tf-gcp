resource "google_compute_network" "tf-network-1" {
  provider = "google-beta"
  name = "website-net"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "tf-subnetwork-1" {
  name = "tf-subnetwork"
  region = "us-central1"
  network = google_compute_network.tf-network.self_link
  ip_cidr_range = "10.10.1.0/24"
}