resource "google_compute_instance" "default" {
  name         = "linux-vm-1"
  machine_type = "f1-micro"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  labels = {
    environment = "development"
    distro = "debian-9"
  }

  network_interface {
    network = google_compute_network.tf-network.self_link
  }
}